FROM digitalocean/doctl:1.64.0

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mkdir -p ~/.local/bin/kubectl && \
    mv ./kubectl /usr/local/bin/kubectl